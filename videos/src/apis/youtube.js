import axios from 'axios';

const KEY = 'AIzaSyDjM88e0ZzPQMhfWJD1k2QOg5WLoxHCV4Q';

export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: 10,
    type: 'video',
    key: `${KEY}`
  }
});
