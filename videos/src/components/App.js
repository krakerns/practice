import React, { Component } from 'react';
import SearchBar from './SearchBar';
import youtube from '../apis/youtube';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';

class App extends Component {
  constructor (props) {
    super(props);
    this.state = { videos: [], selectedItem: null };
  }

  componentDidMount () {
    this.onSearchSubmit('test');
  }

  async onSearchSubmit (term) {
    const response = await youtube.get('/search', {
      params: { q: term }
    });

    this.setState({
      videos: response.data.items,
      selectedItem: response.data.items[0]
    });
    console.log(response.data.items);
  }

  onVideoSelect (video) {
    this.setState({ selectedItem: video });
  }

  render () {
    return (
      <div className='ui container'>
        <SearchBar onSubmit={this.onSearchSubmit.bind(this)} />
        <div className='ui grid'>
          <div className='ui row'>
            <div className='eleven wide column'>
              <VideoDetail video={this.state.selectedItem} />
            </div>

            <div className='five wide column'>
              <VideoList
                videos={this.state.videos}
                onVideoSelect={this.onVideoSelect.bind(this)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
