import React, { Component } from 'react';

class SearchBar extends Component {
  constructor (props) {
    super(props);
    this.state = { term: '' };
  }

  onFormSubmit (e) {
    e.preventDefault();
    this.props.onSubmit(this.state.term);
  }

  render () {
    return (
      <div className='search-bar ui segment'>
        <form className='ui form' onSubmit={this.onFormSubmit.bind(this)}>
          <div className='field'>
            <label>Video Search</label>
            <input
              type='text'
              value={this.state.value}
              onChange={e => {
                this.setState({ term: e.target.value });
              }}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
