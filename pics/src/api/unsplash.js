import axios from 'axios';

export default axios.create({
  headers: {
    Authorization: 'Client-ID 5q6yYRGTTppQP1uqKcw0cm2MhX6XoVHmDlxupQiy9YE'
  },
  baseURL: 'https://api.unsplash.com'
});
