import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts, fetchPostsAndUsers } from '../actions';
import UserHeader from './UserHeader';

export class PostList extends Component {
  componentDidMount () {
    this.props.fetchPostsAndUsers();
  }

  renderList () {
    return this.props.posts.map(post => {
      return (
        <div className='item' key={post.id}>
          <i className='large middle aligned icon user' />
          <div className='content'>
            <h2>{post.title}</h2>
            <div className='description'>{post.body}</div>
            <UserHeader userId={post.userId} />
          </div>
        </div>
      );
    });
  }

  render () {
    console.log(this.props.posts);
    return <div className='ui relaxed divided list'>{this.renderList()}</div>;
  }
}

const mapStateToProps = state => {
  return { posts: state.posts };
};

const mapDispatchToProps = {
  fetchPosts,
  fetchPostsAndUsers
};

export default connect(mapStateToProps, mapDispatchToProps)(PostList);
