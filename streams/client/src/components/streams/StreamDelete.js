import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from '../Modal';
import history from '../../history';
import { fetchStream, deleteStream } from '../../actions';

class StreamDelete extends Component {
  componentDidMount () {
    console.log(this.props.match.params.id);
    this.props.fetchStream(this.props.match.params.id);
  }

  renderActions = () => {
    return (
      <>
        <button
          onClick={() => this.handleDelete(this.props.match.params.id)}
          className='ui button negative'
        >
          Delete
        </button>
        <button onClick={this.handleDismiss} className='ui button cancel'>
          Cancel
        </button>
      </>
    );
  };

  handleDelete = id => {
    this.props.deleteStream(id);
  };

  handleDismiss = () => {
    history.push('/');
  };

  renderContent = () => {
    if (this.props.stream) {
      return 'Are you sure you want to delete this stream?';
    }
    return 'Loading....';
  };

  render () {
    console.log(this.props.stream);
    return (
      <Modal
        title='Delete Stream'
        content={this.renderContent()}
        actions={this.renderActions()}
        onDismiss={this.handleDismiss}
      />
    );
  }
}

const mapStateToProps = (state, { match: { params } }) => {
  // console.log(state.streams[params.id]);
  return {
    stream: state.streams[params.id]
  };
};

const mapDispatchToProps = { fetchStream, deleteStream };

export default connect(mapStateToProps, mapDispatchToProps)(StreamDelete);
