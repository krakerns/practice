import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchStream } from '../../actions';
import flv from 'flv.js';

export class StreamShow extends Component {
  constructor (props) {
    super(props);

    this.videoRef = React.createRef();
  }

  componentDidMount () {
    const {
      match: { params }
    } = this.props;

    this.props.fetchStream(params.id);
    this.buildPlayer();
  }

  componentDidUpdate () {
    this.buildPlayer();
  }

  componentWillUnmount () {
    this.player.destroy();
  }

  buildPlayer = () => {
    if (this.player || !this.props.stream) {
      return;
    }

    const {
      match: { params }
    } = this.props;

    this.player = flv.createPlayer({
      type: 'flv',
      url: `http://localhost:8000/live/${params.id}.flv`
    });

    this.player.attachMediaElement(this.videoRef.current);
    this.player.load();
  };

  render () {
    const { stream } = this.props;

    if (!stream) {
      return <div>Loading..</div>;
    }

    return (
      <div>
        <video ref={this.videoRef} style={{ width: '100%' }} controls />
        <h1>{stream.title}</h1>
        <h5>{stream.description}</h5>
      </div>
    );
  }
}

const mapStateToProps = (state, { match: { params } }) => {
  return {
    stream: state.streams[params.id]
  };
};

const mapDispatchToProps = { fetchStream };

export default connect(mapStateToProps, mapDispatchToProps)(StreamShow);
