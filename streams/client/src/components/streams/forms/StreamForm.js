import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

class StreamForm extends Component {
  renderError = (touched, error) => {
    if (touched && error) {
      return (
        <div className='ui error message'>
          <div className='header'>{error}</div>
        </div>
      );
    }
  };

  renderInput = ({ input, label, meta: { touched, error } }) => {
    const className = `field ${error && touched ? 'error' : ''}`;
    return (
      <div className={className}>
        <label>{label}</label>
        <input {...input} autoComplete='off' />
        {this.renderError(touched, error)}
      </div>
    );
  };

  handleSubmit = values => {
    this.props.onSubmit(values);
  };

  render () {
    return (
      <form
        onSubmit={this.props.handleSubmit(this.handleSubmit)}
        className='ui form error'
      >
        <Field label='Enter Title' name='title' component={this.renderInput} />
        <Field
          label='Enter Description'
          name='description'
          component={this.renderInput}
        />

        <button className='ui button primary'>Submit</button>
      </form>
    );
  }
}

const validate = values => {
  const errors = {};
  if (!values.title) {
    errors.title = 'Required!';
  }

  if (!values.description) {
    errors.description = 'Required!';
  }

  return errors;
};

export default reduxForm({
  form: 'streamForm',
  validate
})(StreamForm);
