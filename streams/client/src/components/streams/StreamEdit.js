import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchStream, editStream } from '../../actions';
import StreamForm from './forms/StreamForm';

export class StreamEdit extends Component {
  componentDidMount () {
    const {
      match: { params }
    } = this.props;

    this.props.fetchStream(params.id);
  }

  handleSubmit = values => {
    const {
      match: { params }
    } = this.props;
    this.props.editStream(params.id, values);
  };

  render () {
    return (
      <div>
        <h3>Edit A Stream</h3>
        <StreamForm
          initialValues={this.props.stream}
          onSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, { match: { params } }) => {
  return {
    stream: state.streams[params.id]
  };
};

const mapDispatchToProps = {
  fetchStream,
  editStream
};

export default connect(mapStateToProps, mapDispatchToProps)(StreamEdit);
