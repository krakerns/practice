import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStream } from '../../actions';
import StreamForm from './forms/StreamForm';

class StreamCreate extends Component {
  handleSubmit = values => {
    this.props.createStream(values);
  };

  render () {
    return (
      <div>
        <h3>Create A Stream</h3>
        <StreamForm onSubmit={this.handleSubmit} />
      </div>
    );
  }
}

export default connect(null, {
  createStream
})(StreamCreate);
