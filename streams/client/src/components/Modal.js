import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Modal extends Component {
  render () {
    const { title, content, actions } = this.props;
    return ReactDOM.createPortal(
      <div
        onClick={this.props.onDismiss}
        className='ui dimmer visible active'
      >
        <div
          onClick={e => e.stopPropagation()}
          className='ui standard modal visible active'
        >
          <div className='header'>{title}</div>
          <div className='content'>
            <p>{content}</p>
          </div>
          <div className='actions'>{actions}</div>
        </div>
      </div>,
      document.querySelector('#modal')
    );
  }
}

export default Modal;
