import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import GoogleAuth from '../components/GoogleAuth'

export class Header extends Component {
  render () {
    return (
      <div className='ui secondary pointing menu'>
        <Link to='/' className='item'>Streamer</Link>
        <div className='right menu'>
          <Link to='/' className='item'>All Streams</Link>
          <Link to='/' className='ui item'><GoogleAuth /></Link>
        </div>
      </div>
    )
  }
}

export default Header
