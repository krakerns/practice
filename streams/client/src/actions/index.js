import {
  SIGN_IN,
  SIGN_OUT,
  CREATE_STREAM,
  FETCH_STREAM,
  FETCH_STREAMS,
  EDIT_STREAM,
  DELETE_STREAM
} from './types';
import streams from '../apis/stream';
import history from '../history';

export const changeAuth = payload => ({
  type: 'CHANGE_AUTH',
  payload
});

export const signIn = payload => ({
  type: SIGN_IN,
  payload
});

export const signOut = payload => ({
  type: SIGN_OUT,
  payload
});

export const createStream = payload => async (dispatch, getState) => {
  const { userId } = getState().auth;
  const response = await streams.post('/streams', { ...payload, userId });
  dispatch({
    type: CREATE_STREAM,
    payload: response.data
  });
  history.push('/');
};

export const fetchStreams = () => async dispatch => {
  const response = await streams.get('/streams');
  dispatch({
    type: FETCH_STREAMS,
    payload: response.data
  });
};

export const fetchStream = payload => async dispatch => {
  const response = await streams.get(`/streams/${payload}`);
  dispatch({
    type: FETCH_STREAM,
    payload: response.data
  });
};

export const editStream = (id, payload) => async dispatch => {
  const response = await streams.put(`/streams/${id}`, payload);
  dispatch({
    type: EDIT_STREAM,
    payload: response.data
  });
  history.push('/');
};

export const deleteStream = payload => async dispatch => {
  await streams.delete(`/streams/${payload}`);
  dispatch({
    type: DELETE_STREAM,
    payload
  });
  history.push('/');
};
